﻿using Acme.Bidding.Core.Domain.Common;
using Acme.Bidding.Core.Domain.DTOs;
using Acme.Bidding.Core.Domain.Entities;
using Acme.Bidding.Core.Domain.Enums;
using Acme.Bidding.Core.Domain.Repositories;
using Acme.Bidding.Core.Domain.Services;
using Acme.Bidding.Core.Domain.Services.Application;
using Acme.Bidding.Core.Domain.Services.Helper;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Acme.Bidding.Core.Application.Services
{
    public class ItemBidService : IItemBidService
    {
        private readonly IAcmeBiddingRepository _acmeBiddingRepository;
        private readonly IMapper _mapper;
        private readonly IBroadcastService _broadcastService;
        private readonly ITokenAttributesService _tokenAttributesService;

        public ItemBidService(IAcmeBiddingRepository acmeBiddingRepository,
            IMapper mapper,
            IBroadcastService broadcastService,
            ITokenAttributesService tokenAttributesService)
        {
            _acmeBiddingRepository = acmeBiddingRepository;
            _mapper = mapper;
            _broadcastService = broadcastService;
            _tokenAttributesService = tokenAttributesService;
        }
        /// <summary>
        /// Add Item Bid Async
        /// </summary>
        /// <param name="itemBid"></param>
        /// <returns>BaseGenericResponse<ItemBidDto></returns>
        public async Task<BaseGenericResponse<decimal>> AddItemBidAsync(ItemBidDto itemBid)
        {

            var higherBids = await _acmeBiddingRepository.FindAllAsync<ItemBid>(bid => bid.ItemId == itemBid.ItemId && bid.BidAmount > itemBid.BidAmount);

            if (higherBids.Any())
            {
                return new BaseGenericResponse<decimal>
                {
                    Message = Constants.ResponseMessages.AddItemBidFailure_LowerBid,
                    IsSuccess = false
                };
            }


            itemBid.UserId = (await _acmeBiddingRepository.FindAsync<User>(user => user.IdentityUserId == _tokenAttributesService.IdentityUserId)).Id;
            var addedBid = await _acmeBiddingRepository.AddAsync(_mapper.Map<ItemBid>(itemBid));

            var result = new BaseGenericResponse<decimal>
            {
                Data = addedBid.BidAmount,
                IsSuccess = true
            }; ;

            JsonConvert.SerializeObject(itemBid, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });


            _broadcastService.BroadcastData(HubMethods.BroadcastItemData.ToString(), JsonConvert.SerializeObject(itemBid, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }));

            return result;
        }

        public async Task<BaseGenericResponse<IEnumerable<ItemBidDto>>> GetBidListByItemIdAsync(int itemId, int? page, int? pageSize)
        {
            IEnumerable<ItemBid> itemBids;

            if (!pageSize.HasValue)
            {
                itemBids = await _acmeBiddingRepository
                   .FindAllIncludingAsync<ItemBid>(bid => bid.ItemId == itemId,
                                                         a => a.Item,
                                                         b => b.User);

                return new BaseGenericResponse<IEnumerable<ItemBidDto>>
                {
                    Data = _mapper.Map<IEnumerable<ItemBidDto>>(itemBids.OrderBy(d => d.BidAmount)),
                    IsSuccess = true
                };
            }


            int recordCount = await _acmeBiddingRepository.FindAllCountAsync<ItemBid>(bid => bid.ItemId == itemId);

            itemBids = await _acmeBiddingRepository.FindAllIncludingWithPagingAsync<ItemBid>(bid => bid.ItemId == itemId,
                ord => ord.BidAmount, false, page.Value, pageSize.Value,
                a => a.Item,
                b => b.User);

            return new BaseGenericResponse<IEnumerable<ItemBidDto>>
            {
                Data = _mapper.Map<IEnumerable<ItemBidDto>>(itemBids),
                IsSuccess = true,
                Paging = new Paging
                {
                    TotalRecords = recordCount,
                    PageNumber = page,
                    PageSize = pageSize,
                }
            };

        }

        public async Task<BaseGenericResponse<IEnumerable<ItemBidDto>>> GetBidListByUserAndItemAsync(int userId, int itemId, int page, int? pageSize)
        {
            IEnumerable<ItemBid> itemBids;

            if (!pageSize.HasValue)
            {
                itemBids = await _acmeBiddingRepository
                   .FindAllIncludingAsync<ItemBid>(bid => (bid.UserId == userId && bid.ItemId == itemId),
                                                         a => a.Item,
                                                         b => b.User);

                return new BaseGenericResponse<IEnumerable<ItemBidDto>>
                {
                    Data = _mapper.Map<IEnumerable<ItemBidDto>>(itemBids.OrderBy(d => d.BidAmount)),
                    IsSuccess = true
                };
            }

            int recordCount = await _acmeBiddingRepository.FindAllCountAsync<ItemBid>(bid => (bid.UserId == userId && bid.ItemId == itemId));

            itemBids = await _acmeBiddingRepository.FindAllIncludingWithPagingAsync<ItemBid>(bid => (bid.UserId == userId && bid.ItemId == itemId),
                ord => ord.BidAmount, false, page, pageSize.Value,
                a => a.Item,
                b => b.User);

            return new BaseGenericResponse<IEnumerable<ItemBidDto>>
            {
                Data = _mapper.Map<IEnumerable<ItemBidDto>>(itemBids),
                IsSuccess = true,
                Paging = new Paging
                {
                    TotalRecords = recordCount,
                    PageNumber = page,
                    PageSize = pageSize,
                }
            };

        }

        public async Task<BaseGenericResponse<IEnumerable<ItemBidDto>>> GetBidListByUserIdAsync(int userId, int? page, int? pageSize)
        {
            IEnumerable<ItemBid> itemBids;

            if (!pageSize.HasValue)
            {
                itemBids = await _acmeBiddingRepository
                   .FindAllIncludingAsync<ItemBid>(bid => bid.UserId == userId,
                                                         a => a.Item,
                                                         b => b.User);

                return new BaseGenericResponse<IEnumerable<ItemBidDto>>
                {
                    Data = _mapper.Map<IEnumerable<ItemBidDto>>(itemBids.OrderBy(d => d.BidAmount)),
                    IsSuccess = true
                };
            }

            int recordCount = await _acmeBiddingRepository.FindAllCountAsync<ItemBid>(bid => bid.UserId == userId);

            itemBids = await _acmeBiddingRepository.FindAllIncludingWithPagingAsync<ItemBid>(bid => bid.UserId == userId,
                ord => ord.BidAmount, false, page.Value, pageSize.Value,
                a => a.Item,
                b => b.User);

            return new BaseGenericResponse<IEnumerable<ItemBidDto>>
            {
                Data = _mapper.Map<IEnumerable<ItemBidDto>>(itemBids),
                IsSuccess = true,
                Paging = new Paging
                {
                    TotalRecords = recordCount,
                    PageNumber = page,
                    PageSize = pageSize,
                }
            };
        }
    }
}
