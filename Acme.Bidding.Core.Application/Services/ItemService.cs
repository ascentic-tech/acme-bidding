﻿using Acme.Bidding.Core.Domain.Common;
using Acme.Bidding.Core.Domain.DTOs;
using Acme.Bidding.Core.Domain.Entities;
using Acme.Bidding.Core.Domain.Enums;
using Acme.Bidding.Core.Domain.Repositories;
using Acme.Bidding.Core.Domain.Services.Application;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Acme.Bidding.Core.Application.Services
{
    public class ItemService : IItemService
    {
        private readonly IAcmeBiddingRepository _acmeBiddingRepository;
        private readonly IMapper _mapper;

        public ItemService(IAcmeBiddingRepository acmeBiddingRepository, IMapper mapper)
        {
            _acmeBiddingRepository = acmeBiddingRepository;
            _mapper = mapper;
        }

        public async Task<BaseGenericResponse<ItemDto>> GetItemByIdAsync(int id)
        {
            var item = await _acmeBiddingRepository.FindIncludingAsync<Item>(itm => itm.Id == id,
                a => a.ItemBids,
                b => b.ItemImages);

            if (item == null)
            {
                return new BaseGenericResponse<ItemDto>
                {
                    IsSuccess = false,
                    Message = "Item not found."
                };
            }

            ItemDto itemDto = _mapper.Map<ItemDto>(item);
            itemDto.HighestBid = itemDto.ItemBids.Any() ? itemDto.ItemBids.Select(bid => bid.BidAmount).Max() : 0;
            return new BaseGenericResponse<ItemDto>
            {
                Data = itemDto,
                IsSuccess = true
            };
        }
      
         public async Task<BaseGenericResponse<IEnumerable<ItemDto>>> GetItemListByStatusAsync(int itemStatus, int page, int? pageSize)
        {
            IEnumerable<Item> items;
            if (!pageSize.HasValue)
            {
                items = await _acmeBiddingRepository
                   .FindAllIncludingAsync<Item>(item => item.ItemStatus == (ItemStatus)itemStatus && item.Duration >= DateTime.Now,
                                                         a => a.ItemBids,
                                                         b => b.ItemImages);

                IEnumerable<ItemDto> itemDtos1 = _mapper.Map<IEnumerable<ItemDto>>(items.OrderBy(d => d.StartingTime));
                foreach (var item in itemDtos1.Where(item => item.ItemBids.Any()))
                {
                    item.HighestBid = item.ItemBids.Select(bid => bid.BidAmount).Max();
                }

                return new BaseGenericResponse<IEnumerable<ItemDto>>
                {
                    Data = itemDtos1,
                    IsSuccess = true
                };

            }

            int recordCount = await _acmeBiddingRepository.FindAllCountAsync<Item>(item => item.ItemStatus == (ItemStatus)itemStatus && item.Duration >= DateTime.Now);

            items = await _acmeBiddingRepository.FindAllIncludingWithPagingAsync<Item>(item =>
                item.ItemStatus == (ItemStatus)itemStatus && item.Duration >= DateTime.Now,
                ord => ord.StartingTime, false, page, pageSize.Value,
                a => a.ItemBids,
                b => b.ItemImages);


            IEnumerable<ItemDto> itemDtos2 = _mapper.Map<IEnumerable<ItemDto>>(items.OrderBy(d => d.StartingTime));

            foreach (var item in itemDtos2.Where(item => item.ItemBids.Any()))
            {
                item.HighestBid = item.ItemBids.Select(bid => bid.BidAmount).Max();
            }

            return new BaseGenericResponse<IEnumerable<ItemDto>>
            {
                Data = itemDtos2,
                IsSuccess = true,
                Paging = new Paging
                {
                    TotalRecords = recordCount,
                    PageNumber = page,
                    PageSize = pageSize,
                }
            };
        }

        public async Task<BaseGenericResponse<IEnumerable<ItemDto>>> GetAllItemListAsync(int page, int? pageSize)
        {
            IEnumerable<Item> items;
            if (!pageSize.HasValue)
            {
                items = await _acmeBiddingRepository.GetAllIncludingAsync<Item>(a => a.ItemBids, b => b.ItemImages);

                return new BaseGenericResponse<IEnumerable<ItemDto>>
                {
                    Data = _mapper.Map<IEnumerable<ItemDto>>(items.OrderBy(d => d.StartingTime)),
                    IsSuccess = true
                };
            }

            int recordCount = await _acmeBiddingRepository.FindAllCountAsync<Item>();

            items = await _acmeBiddingRepository.GetAllIncludingWithPagingAsync<Item>(page,
                pageSize.Value,
                a => a.ItemBids,
                b => b.ItemImages);

            return new BaseGenericResponse<IEnumerable<ItemDto>>
            {
                Data = _mapper.Map<IEnumerable<Item>, List<ItemDto>>(items.OrderBy(d => d.StartingTime)),
                IsSuccess = true,
                Paging = new Paging
                {
                    TotalRecords = recordCount,
                    PageNumber = page,
                    PageSize = pageSize,
                }
            };
        }

        public async Task<ItemDto> AddItemAsync(ItemDto item)
        {
            var addedItem = await _acmeBiddingRepository.AddAsync(_mapper.Map<Item>(item));
            return _mapper.Map<ItemDto>(addedItem);
        }

        public async Task<ItemDto> UpdateItemAsync(int id, ItemDto item)
        {
            var updatedItem = await _acmeBiddingRepository.UpdateAsync(id, _mapper.Map<Item>(item));
            return _mapper.Map<ItemDto>(updatedItem);
        }
    }
}