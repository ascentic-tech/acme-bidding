﻿using Acme.Bidding.Core.Domain.Common;
using Acme.Bidding.Core.Domain.DTOs;
using Acme.Bidding.Core.Domain.Entities;
using Acme.Bidding.Core.Domain.Repositories;
using Acme.Bidding.Core.Domain.Services.Application;
using AutoMapper;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Acme.Bidding.Core.Application.Services
{
    public class UserService : IUserService
    {
        private readonly IAcmeBiddingRepository _acmeBiddingRepository;
        private readonly IMapper _mapper;
        private readonly IHttpClientFactory _clientFactory;


        public UserService(IAcmeBiddingRepository acmeBiddingRepository, IMapper mapper, IHttpClientFactory clientFactory)
        {
            _acmeBiddingRepository = acmeBiddingRepository;
            _mapper = mapper;
            _clientFactory = clientFactory;
        }

        public async Task<BaseGenericResponse<UserDto>> AddUserAsync(UserRegistrationDto userRegistrationDto)
        {
            userRegistrationDto = await RegisterIdentityUserAsync(userRegistrationDto);
            if (userRegistrationDto.IdentityUserId == Guid.Empty)
            {
                return new BaseGenericResponse<UserDto>
                {
                    Data = null,
                    Message = Constants.ResponseMessages.AddUserFailure_Identity,
                    IsSuccess = false
                };
            }

            User user = _mapper.Map<User>(userRegistrationDto);
            user = await _acmeBiddingRepository.AddAsync(user);


            UserRole userRole = new UserRole
            {
                RoleId = 2,
                UserId = user.Id,
                IsActive = true
            };

            await _acmeBiddingRepository.AddAsync(userRole);
            UserDto userDto = _mapper.Map<UserDto>(user);

            return new BaseGenericResponse<UserDto>
            {
                Data = userDto,
                IsSuccess = true
            };
        }

        private async Task<UserRegistrationDto> RegisterIdentityUserAsync(UserRegistrationDto userRegistrationDto)
        {
            try
            {
                userRegistrationDto.UserName = userRegistrationDto.Email;
                HttpResponseMessage identityUserResponse = await _clientFactory.CreateClient("AuthServiceApi")
                        .PostAsync("Register", new StringContent(JsonConvert.SerializeObject(userRegistrationDto), Encoding.UTF8, "application/json"));

                if (identityUserResponse.IsSuccessStatusCode)
                {
                    string resultUser = await identityUserResponse.Content.ReadAsStringAsync();
                    userRegistrationDto.IdentityUserId = Guid.Parse(resultUser);
                    return userRegistrationDto;
                }
                userRegistrationDto.IdentityUserId = Guid.Empty;
                return userRegistrationDto;
            }
            catch (Exception ex)
            {
                return userRegistrationDto;
            }
        }


        public Task<UserDto> UpdateUserAsync(int id, UserDto user)
        {
            throw new System.NotImplementedException();
        }
    }
}
