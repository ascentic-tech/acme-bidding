*** Steps to launch the appliation ***

1. Clone or download the following project from following git urls

Angular Application: https://gitlab.com/ascentic-tech/acme-bidding-presentation
WebAPI: https://gitlab.com/ascentic-tech/acme-bidding
 
2. Run the following DB migrations on Identity Server project (acme-bidding\Acme.Bidding.Auth.IdentityServer) to setup the Database 

Add-Migration InitialPersistedGrantDbMigration -context PersistedGrantDbContext -output Migrations/PersistedGrantDb
Add-Migration InitialConfigurationDbMigration -context ConfigurationDbContext -output Migrations/ConfigurationDb
Add-Migration InitialAcmeIdentityDbMigration -context AcmeIdentityDbContext -output Migrations/AcmeIdentityDb

Update-Database -context PersistedGrantDbContext 
Update-Database -context ConfigurationDbContext 
Update-Database -context AcmeIdentityDbContext

3. Run the following data migrtions to on web API project(acme-bidding\Acme.Bidding.WebAPI) to setup the Database 

Add-Migration InitialAcmeBiddingDbMigration -context AcmeBiddingDbContext -output Migrations
Update-Database -context AcmeBiddingDbContext

PLEASE NOTE that, the connection strings are currently pointed to LocalDB.
Eg.   Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = AcmeBiddingAppDb

4. Run the sql script (dbo.InitialSeed.sql) in DataSeed folder of "acme-bidding\Acme.Bidding.Core.Domain", to insert master data and initial required mappings to utilize the application.

5. Start the angular application by runing the folowing command on command prompt.
   1. npm install
   2. ng start or ng serve 

6. Next execute the web api project and Identity server project.

7. Once all the projects are Up without any issues then use following credentials to test the project.
  
   Username:  ben@b.com        Password: Abc@12345
   Username:  jhon@j.com       Password: Abc@12345
   Username:  bob@b.com        Password: Abc@12345
   Username:  alice@a.com      Password: Abc@12345




