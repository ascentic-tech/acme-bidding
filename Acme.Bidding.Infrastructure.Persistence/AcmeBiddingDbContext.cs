﻿using Acme.Bidding.Core.Domain.Common;
using Acme.Bidding.Core.Domain.Entities;
using Acme.Bidding.Core.Domain.Services.Helper;
using Acme.Bidding.Infrastructure.Persistence.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Acme.Bidding.Infrastructure.Persistence
{
    public class AcmeBiddingDbContext : DbContext
    {
        private readonly ITokenAttributesService _tokenAttributesService;
        public AcmeBiddingDbContext(DbContextOptions<AcmeBiddingDbContext> options, ITokenAttributesService tokenAttributesService) : base(options)
        {
            _tokenAttributesService = tokenAttributesService;
        }

        public DbSet<UserNotification> UserNotifications { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<NotificationTemplate> NotificationTemplates { get; set; }
        public DbSet<NotificationType> NotificationTypes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<ItemBid> ItemBids { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<ItemImage> ItemImages { get; set; }


        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            TrackChanges();
            return await base.SaveChangesAsync(cancellationToken);
        }


        private void TrackChanges()
        {
            int userId = 1;
            if (_tokenAttributesService.IdentityUserId == Guid.Empty)
            {
                userId = Users.FirstOrDefault(user => user.IdentityUserId == _tokenAttributesService.IdentityUserId).Id;
            }

            foreach (var entity in ChangeTracker.Entries().Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
            {
                if (entity.Entity is IAuditable result)
                {
                    ((IAuditable)entity.Entity).LastModifiedAt = DateTime.Now;
                    if (entity.State == EntityState.Added)
                    {
                        ((IAuditable)entity.Entity).CreatedBy = userId;
                        ((IAuditable)entity.Entity).CreatedAt = DateTime.Now;
                    }
                    else
                    {
                        ((IAuditable)entity.Entity).LastModifiedBy = userId;
                    }
                }
            }
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.CreateEnumLookupTable(createForeignKeys: true);
            modelBuilder.OverrideDeleteBehaviour(DeleteBehavior.Restrict);
            base.OnModelCreating(modelBuilder);
        }
    }
}