﻿using Acme.Bidding.Core.Domain.Common;
using Acme.Bidding.Core.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Acme.Bidding.Infrastructure.Persistence.Repositories
{
    public class AcmeBiddingRepository : IAcmeBiddingRepository
    {
        private readonly AcmeBiddingDbContext _dbContext;

        public AcmeBiddingRepository(AcmeBiddingDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<T> AddAsync<T>(T entity) where T : BaseEntity
        {
            await _dbContext.Set<T>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();

            return entity;
        }

        public async Task<IEnumerable<T>> AddRangeAsync<T>(IEnumerable<T> entities) where T : BaseEntity
        {
            await _dbContext.Set<T>().AddRangeAsync(entities);
            await _dbContext.SaveChangesAsync();
            return entities;
        }

        public async Task DeleteAsync<T>(T entity) where T : BaseEntity
        {
            _dbContext.Set<T>().Remove(entity);
             await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteRange<T>(IEnumerable<T> entities) where T : BaseEntity
        {
            _dbContext.Set<T>().RemoveRange(entities);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> FindAllAsync<T>(Expression<Func<T, bool>> predicate) where T : BaseEntity
        {
            return await _dbContext.Set<T>().Where(predicate).ToListAsync();
        }

        public async Task<IEnumerable<T>> FindAllWithPagingAsync<T>(Expression<Func<T, bool>> predicate, Expression<Func<T, object>> orderBy, bool orderByAscending, int page, int pageSize) where T : BaseEntity
        {
            if (orderByAscending)
            {
                return await _dbContext.Set<T>().OrderBy(orderBy).Where(predicate).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
            }
            else
            {
                return await _dbContext.Set<T>().OrderByDescending(orderBy).Where(predicate).Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
            }
        }

        public async Task<IEnumerable<T>> FindAllIncludingWithPagingAsync<T>(Expression<Func<T, bool>> predicate, Expression<Func<T, object>> orderBy, bool orderByAscending, int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity
        {
            IQueryable<T> query;
            if (orderByAscending)
            {
                query = _dbContext.Set<T>().OrderBy(orderBy).Where(predicate).Skip((page - 1) * pageSize).Take(pageSize);
            }
            else
            {
                query = _dbContext.Set<T>().OrderByDescending(orderBy).Where(predicate).Skip((page - 1) * pageSize).Take(pageSize);
            }

            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return await query.ToListAsync();
        }

        public IEnumerable<T> FindAllIncludingWithPagingGroupBy<T>(Expression<Func<T, bool>> predicate, Expression<Func<T, object>> orderBy, Expression<Func<T, object>> groupBy, bool orderByAscending, int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity
        {
            IQueryable<T> query;
            if (orderByAscending)
            {
                query = _dbContext.Set<T>().OrderBy(orderBy).Where(predicate);
            }
            else
            {
                query = _dbContext.Set<T>().OrderByDescending(orderBy).Where(predicate);
            }

            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query.GroupBy(groupBy.Compile()).Select(x => x.FirstOrDefault()).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public IEnumerable<IGrouping<object, T>> FindAllIncludingWithGroupBy<T>(Expression<Func<T, bool>> predicate, Expression<Func<T, object>> groupBy, params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity
        {
            IQueryable<T> query = _dbContext.Set<T>().Where(predicate);
            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query.GroupBy(groupBy.Compile());
        }

        public async Task<T> FindAsync<T>(Expression<Func<T, bool>> predicate) where T : BaseEntity
        {
            return await _dbContext.Set<T>().SingleOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<T>> FindAllIncludingAsync<T>(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity
        {
            IQueryable<T> query = _dbContext.Set<T>().Where(predicate).AsQueryable();
            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return await query.ToListAsync();
        }

        public async Task<T> FindIncludingAsync<T>(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity
        {
            IQueryable<T> query = _dbContext.Set<T>().Where(predicate).AsQueryable();
            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return await query.FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetAllAsync<T>() where T : BaseEntity
        {
            return await _dbContext.Set<T>().ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllWithPagingAsync<T>(int page, int pageSize) where T : BaseEntity
        {
            return await _dbContext.Set<T>().Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllIncludingAsync<T>(params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity
        {
            IQueryable<T> queryable = _dbContext.Set<T>().AsQueryable();
            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                queryable = queryable.Include(includeProperty);
            }

            return await queryable.ToListAsync();
        }

        public async Task<IEnumerable<T>> GetAllIncludingWithPagingAsync<T>(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity
        {
            IQueryable<T> queryable = _dbContext.Set<T>().Skip((page - 1) * pageSize).Take(pageSize);
            foreach (Expression<Func<T, object>> includeProperty in includeProperties)
            {
                queryable = queryable.Include(includeProperty);
            }

            return await queryable.ToListAsync();
        }

        public Task<T> GetByIdAsync<T>(int id) where T : BaseEntity
        {
            return _dbContext.Set<T>().SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<T> UpdateAsync<T>(object key, T entity) where T : BaseEntity
        {
            if (entity == null)
            {
                return null;
            }

            T exist = await _dbContext.Set<T>().FindAsync(key);
            if (exist != null)
            {
                if (entity is IAuditable)
                {
                    object createdBy = exist.GetType().GetProperty("CreatedBy").GetValue(exist);
                    object createdOn = exist.GetType().GetProperty("CreatedOn").GetValue(exist);
                    entity.GetType().GetProperty("CreatedBy").SetValue(entity, createdBy);
                    entity.GetType().GetProperty("CreatedOn").SetValue(entity, createdOn);
                }
                _dbContext.Entry(exist).CurrentValues.SetValues(entity);
            }

            await _dbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<int> FindAllCountAsync<T>(Expression<Func<T, bool>> predicate) where T : BaseEntity
        {
            return await _dbContext.Set<T>().Where(predicate).CountAsync();
        }

        public async Task<int> FindAllCountAsync<T>() where T : BaseEntity
        {
            return await _dbContext.Set<T>().CountAsync();
        }
    }
}
