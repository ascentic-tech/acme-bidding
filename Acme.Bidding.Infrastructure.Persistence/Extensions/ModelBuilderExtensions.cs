﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Linq;

namespace Acme.Bidding.Infrastructure.Persistence.Extensions 
{ 
    public static class ModelBuilderExtensions
    {
        public static void OverrideDeleteBehaviour(this ModelBuilder modelBuilder, DeleteBehavior deleteBehaviour = DeleteBehavior.Restrict)
        {
            foreach (IMutableForeignKey relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = deleteBehaviour;
            }
        }
    }
}
