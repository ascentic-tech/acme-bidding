﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Acme.Bidding.Infrastructure.Persistence.Extensions
{
    /// <summary>
    /// Generic entity representing a enum.
    /// </summary>
    /// <typeparam name="T">Enum type.</typeparam>
    public class EnumLookup<T>
        where T : Enum
    {
        public EnumLookup()
        {
        }
        public EnumLookup(T value)
        {
            Id = Convert.ToInt32(value);
            Value = value;
            Name = value.ToString();
        }

        public int Id { get; set; }
        public T Value { get; set; }
        public string Name { get; set; }
    }

    public static class EnumLookupExtensions
    {
        /// <summary>
        /// Scan all registered entities and build a enum lookup table for any enum properties.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        /// <param name="createForeignKeys">Create foreign keys. Note that default delete behavior of EF is Cascade, so changes in enums might delete your data!</param>
        public static void CreateEnumLookupTable(this ModelBuilder modelBuilder, bool createForeignKeys = false)
        {
            List<string> addedEnums = new List<string>();
            foreach (IMutableProperty property in modelBuilder.Model.GetEntityTypes().SelectMany(t => t.GetProperties()).ToArray())
            {
                IMutableEntityType entityType = property.DeclaringEntityType;
                Type propertyType = property.ClrType;

                if (!propertyType.IsEnum)
                {
                    continue;
                }

                Type concreteType = typeof(EnumLookup<>).MakeGenericType(propertyType);
                EntityTypeBuilder enumLookupBuilder = modelBuilder.Entity(concreteType);
                enumLookupBuilder.HasAlternateKey(nameof(EnumLookup<Enum>.Value));
                object[] data = Enum.GetValues(propertyType).Cast<object>()
                    .Select(v => Activator.CreateInstance(concreteType, new object[] { v }))
                    .ToArray();
                if (!addedEnums.Exists(n => n.Contains(concreteType.ToString())))
                {
                    enumLookupBuilder.HasData(data);
                    addedEnums.Add(concreteType.ToString());
                }

                if (createForeignKeys)
                {
                    modelBuilder.Entity(entityType.Name)
                        .HasOne(concreteType)
                        .WithMany()
                        .HasPrincipalKey(nameof(EnumLookup<Enum>.Value))
                        .HasForeignKey(property.Name)
                        .OnDelete(DeleteBehavior.ClientNoAction);
                }
            }
        }
    }
}
