using Acme.Bidding.Core.Application.Services;
using Acme.Bidding.Core.Domain.Entities;
using Acme.Bidding.Core.Domain.Mappings;
using Acme.Bidding.Core.Domain.Repositories;
using AutoMapper;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Acme.Bidding.NUnit
{
    [TestFixture]
    public class ItemBidServiceTest
    {
        private Mock<IAcmeBiddingRepository> mockAcmeBiddingRepository;
        private IMapper mapper;
        [SetUp]
        public void Setup()
        {
            mockAcmeBiddingRepository = new Mock<IAcmeBiddingRepository>();
            MapperConfiguration mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            mapper = mappingConfig.CreateMapper();
        }

        [Test]
        [TestCase(1, true, true)]
        [TestCase(2, true, false)]
        public async Task ItemBidService_GetBidListByItemIdAsync_ReturnListOfItemBids(int itemId, bool isSuccess, bool expectedResult)
        {
            mockAcmeBiddingRepository.Setup(a => a.FindAllIncludingAsync<ItemBid>(bid => bid.ItemId == itemId,
                                                         a => a.Item,
                                                         b => b.User)).ReturnsAsync(new List<ItemBid>
                                                         {
                                                             new ItemBid
                                                             {
                                                                 Id =1,
                                                                 UserId=1,
                                                                 ItemId=1,
                                                                 BidAmount=100,
                                                                 BiddedAt = new DateTime(2021,2,4,14,0,0),
                                                                 CreatedAt= new DateTime(2021,2,4,14,0,0),
                                                                 CreatedBy=1
                                                             },new ItemBid
                                                             {
                                                                 Id =2,
                                                                 UserId=2,
                                                                 ItemId=1,
                                                                 BidAmount=100,
                                                                 BiddedAt = new DateTime(2021,2,4,15,0,0),
                                                                 CreatedAt= new DateTime(2021,2,4,15,0,0),
                                                                 CreatedBy=2
                                                             }
                                                         });

            var itemBidService = new ItemBidService(mockAcmeBiddingRepository.Object, mapper, null, null);
            var result = await itemBidService.GetBidListByItemIdAsync(itemId, null, null);

            Assert.That(result.IsSuccess, Is.EqualTo(isSuccess));
            Assert.That(result.Data.All(a => a.ItemId == itemId), Is.EqualTo(expectedResult));
        }


        [Test]
        [TestCase(1, true, true)]
        [TestCase(2, true, false)]
        public async Task ItemBidService_GetBidListByUserIdAsync_ReturnListOfItemBids(int userId, bool isSuccess, bool expectedResult)
        {
            mockAcmeBiddingRepository.Setup(a => a.FindAllIncludingAsync<ItemBid>(bid => bid.UserId == userId,
                                                         a => a.Item,
                                                         b => b.User)).ReturnsAsync(new List<ItemBid>
                                                         {
                                                             new ItemBid
                                                             {
                                                                 Id =1,
                                                                 UserId=1,
                                                                 ItemId=2,
                                                                 BidAmount=100,
                                                                 BiddedAt = new DateTime(2021,2,4,14,0,0),
                                                                 CreatedAt= new DateTime(2021,2,4,14,0,0),
                                                                 CreatedBy=1
                                                             },new ItemBid
                                                             {
                                                                 Id =2,
                                                                 UserId=1,
                                                                 ItemId=2,
                                                                 BidAmount=100,
                                                                 BiddedAt = new DateTime(2021,2,4,15,0,0),
                                                                 CreatedAt= new DateTime(2021,2,4,15,0,0),
                                                                 CreatedBy=2
                                                             }
                                                         });

            var itemBidService = new ItemBidService(mockAcmeBiddingRepository.Object, mapper, null, null);
            var result = await itemBidService.GetBidListByUserIdAsync(userId, null, null);

            Assert.That(result.IsSuccess, Is.EqualTo(isSuccess));
            Assert.That(result.Data.All(a => a.UserId == userId), Is.EqualTo(expectedResult));
        }
    }
}