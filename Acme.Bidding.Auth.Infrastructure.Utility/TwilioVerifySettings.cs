﻿namespace Acme.Bidding.Auth.Infrastructure.Utility
{
    public class TwilioVerifySettings
    {
        public string VerificationServiceSID { get; set; }
    }
}
