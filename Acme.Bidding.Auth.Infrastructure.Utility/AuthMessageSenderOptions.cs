﻿namespace Acme.Bidding.Auth.Infrastructure.Utility
{
    public class AuthMessageSenderOptions
    {
        public string SendGridKey { get; set; }
    }
}
