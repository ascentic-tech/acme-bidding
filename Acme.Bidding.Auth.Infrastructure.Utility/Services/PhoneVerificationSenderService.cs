﻿using System.Threading.Tasks;
using Acme.Bidding.Auth.Core.Domain.Services;
using Microsoft.Extensions.Options;
using Twilio.Rest.Verify.V2.Service;

namespace Acme.Bidding.Auth.Infrastructure.Utility.Services
{
    public class PhoneVerificationSenderService : IPhoneVerificationSenderService
    {
        private readonly TwilioVerifySettings _settings;

        public PhoneVerificationSenderService(IOptions<TwilioVerifySettings> settings)
        {
            _settings = settings.Value;
        }

        public async Task<VerificationResource> SendVeryficationCode(string phoneNumber)
        {
            return await VerificationResource.CreateAsync(
                to: phoneNumber,
                channel: "sms",
                pathServiceSid: _settings.VerificationServiceSID
            );
        }

        public async Task<VerificationCheckResource> VerifyCode(string phoneNumber, string code)
        {
            return await VerificationCheckResource.CreateAsync(
                 to: phoneNumber,
                 code: code,
                 pathServiceSid: _settings.VerificationServiceSID
             );
        }
    }
}
