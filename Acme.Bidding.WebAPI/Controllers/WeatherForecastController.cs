﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Acme.Bidding.WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiVersion("1.0")]
    [ApiVersion("1.1")]
    [ApiController]
    [Authorize]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }


        /// <summary>
        /// Gets WeatherForecast.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetWeatherForecast")]
        public IEnumerable<WeatherForecast> GetWeatherForecast_V1()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }


        /// <summary>
        /// Gets WeatherForecast.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [MapToApiVersion("1.1")]
        [ActionName("GetWeatherForecast")]
        public IEnumerable<WeatherForecast> GetWeatherForecast_V2()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
