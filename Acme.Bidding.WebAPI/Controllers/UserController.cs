﻿using Acme.Bidding.Core.Domain.Common;
using Acme.Bidding.Core.Domain.DTOs;
using Acme.Bidding.Core.Domain.Services.Application;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Acme.Bidding.WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiVersion("1.0")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            this._userService = userService;
        }


        /// <summary>
        /// User Registration
        /// </summary>
        /// <param name="userRegistrationDto"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("AddUserAsync")]
        public async Task<ActionResult<BaseGenericResponse<UserDto>>> AddUserAsync([FromBody] UserRegistrationDto userRegistrationDto)
        {
            try
            {
                var response = await _userService.AddUserAsync(userRegistrationDto);

                if (response.IsSuccess)
                {
                    return Ok(response);
                }

                return BadRequest(response);
            }
            catch (Exception)
            {
                return Conflict("Something went wrong.");
            }
        }
    }
}
