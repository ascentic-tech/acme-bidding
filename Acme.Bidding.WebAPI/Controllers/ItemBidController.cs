﻿using Acme.Bidding.Core.Domain.Attributes;
using Acme.Bidding.Core.Domain.DTOs;
using Acme.Bidding.Core.Domain.Services.Application;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using static Acme.Bidding.Core.Domain.Common.Constants;

namespace Acme.Bidding.WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiVersion("1.0")]
    [ApiController]
    [Authorize]
    public class ItemBidController : Controller
    {
        private readonly IItemBidService _itemBidService;

        public ItemBidController(IItemBidService itemBidService)
        {
            _itemBidService = itemBidService;
        }

        /// <summary>
        /// Get bids of an item with optional pagination.
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetBidListByItemIdAsync")]
        [AcmeAuthorize("ITEM_GET_BIDLISTBYITEMID")]
        public async Task<IActionResult> GetBidListByItemIdAsync(int itemId, int? page, int? pageSize)
        {
            try
            {
                var itemBids = await _itemBidService.GetBidListByItemIdAsync(itemId, page, pageSize);

                if (!itemBids.Data.Any())
                {
                    return NotFound(ResponseMessages.ItemBidsNotFound);
                }

                return Ok(itemBids);
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Get bids of specific user with optional pagination.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetBidListByUserIdAsync")]
        [AcmeAuthorize("ITEM_GET_BIDLISTBYUSERID")]
        public async Task<IActionResult> GetBidListByUserIdAsync(int userId, int? page, int? pageSize)
        {
            try
            {
                var itemBids = await _itemBidService.GetBidListByUserIdAsync(userId, page, pageSize);

                if (!itemBids.Data.Any())
                {
                    return NotFound(ResponseMessages.ItemBidsNotFound);
                }

                return Ok(itemBids);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        ///  Get bids based on user and item with optional pagination.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="itemId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetBidListByUserAndItemAsync")]
        [AcmeAuthorize("ITEM_GET_BIDLISTBYUSERANDITEM")]
        public async Task<IActionResult> GetBidListByUserAndItemAsync(int userId, int itemId, int page, int? pageSize)
        {
            try
            {
                var itemBids = await _itemBidService.GetBidListByUserAndItemAsync(userId, itemId, page, pageSize);

                if (!itemBids.Data.Any())
                {
                    return NotFound(ResponseMessages.ItemBidsNotFound);
                }

                return Ok(itemBids);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemBid"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("AddItemBidAsync")]
        public async Task<IActionResult> AddItemBidAsync([FromBody] ItemBidDto itemBid)
        {
            try
            {
                var response = await _itemBidService.AddItemBidAsync(itemBid);

                if (!response.IsSuccess)
                {
                    return BadRequest(response);
                }

                return Ok(response);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}