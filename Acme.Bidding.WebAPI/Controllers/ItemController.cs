﻿using Acme.Bidding.Core.Domain.Attributes;
using Acme.Bidding.Core.Domain.Common;
using Acme.Bidding.Core.Domain.DTOs;
using Acme.Bidding.Core.Domain.Services.Application;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Acme.Bidding.Core.Domain.Common.Constants;

namespace Acme.Bidding.WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiVersion("1.0")]
    [ApiController]

    public class ItemController : Controller
    {
        private readonly IItemService _itemService;

        public ItemController(IItemService itemService)
        {
            _itemService = itemService;
        }


        /// <summary>
        /// Get item by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [ActionName("GetItemByIdAsync")]
        public async Task<ActionResult<BaseGenericResponse<ItemDto>>> GetItemByIdAsync([FromRoute] int id)
        {
            try
            {
                var item = await _itemService.GetItemByIdAsync(id);
                return Ok(item);
            }
            catch (Exception ex)
            {
                return Conflict("Something went wrong.");
            }
        }



        /// <summary>
        /// Get all items with optional pagination
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetAllItemListAsync")]
        [Authorize]
        [AcmeAuthorize("ITEM_GET_ALLITEMLIST")]
        public async Task<ActionResult<BaseGenericResponse<IEnumerable<ItemDto>>>> GetAllItemListAsync(int page, int? pageSize)
        {
            try
            {
                var items = await _itemService.GetAllItemListAsync(page, pageSize);

                if (!items.IsSuccess)
                {
                    return NotFound(ResponseMessages.ItemsNotFound);
                }

                return Ok(items);
            }
            catch (Exception)
            {
                throw;               
            }
        }


        /// <summary>
        /// Get items based on status and optional pagination
        /// </summary>
        /// <param name="itemStatus"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("GetItemListByStatusAsync")]
        public async Task<ActionResult<BaseGenericResponse<IEnumerable<ItemDto>>>> GetItemListByStatusAsync(int itemStatus, int page, int? pageSize)
        {
            try
            {
                var items = await _itemService.GetItemListByStatusAsync(itemStatus, page, pageSize);

                if (!items.Data.Any())
                {
                    return NotFound(ResponseMessages.ItemsNotFound);
                }

                return Ok(items);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        /// <summary>
        /// Add Item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [AcmeAuthorize("ITEM_ADD_ITEM")]
        public async Task<IActionResult> AddItemAsync([FromBody] ItemDto item)
        {
            try
            {
                item = await _itemService.AddItemAsync(item);

                if (item.Id == 0)
                {
                    return BadRequest(ResponseMessages.ItemsNotSaved);
                }

                return Ok(item);
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// Update Item
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize]
        [AcmeAuthorize("ITEM_UPDATE_ITEM")]
        public async Task<IActionResult> UpdateItemAsync(int id, [FromBody] ItemDto item)
        {
            try
            {
                item = await _itemService.UpdateItemAsync(id, item);

                if (item.Id == 0)
                {
                    return BadRequest(ResponseMessages.ItemsNotUpdated);
                }

                return Ok(item);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
