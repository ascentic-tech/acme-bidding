﻿using Acme.Bidding.Core.Domain.Configurations;
using Acme.Bidding.Core.Domain.Configurations.Swagger;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Acme.Bidding.WebAPI.Configuration
{
    /// <summary>
    /// Registers all the settings from configuration file in the IOC container
    /// </summary>
    public static class ConfigurationOptions
    {
        public static void ConfigureService(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<AppSettings>(configuration.GetSection(DefaultConstants.AppSettings));
            services.AddSingleton(resolver => resolver.GetRequiredService<IOptions<AppSettings>>().Value);
            services.AddSingleton<IValidatable>(resolver => resolver.GetRequiredService<IOptions<AppSettings>>().Value);

            services.Configure<SwaggerSettings>(configuration.GetSection(DefaultConstants.SwaggerSettings));
            services.AddSingleton(resolver => resolver.GetRequiredService<IOptions<SwaggerSettings>>().Value);
            services.AddSingleton<IValidatable>(resolver => resolver.GetRequiredService<IOptions<SwaggerSettings>>().Value);
        }
    }
}
