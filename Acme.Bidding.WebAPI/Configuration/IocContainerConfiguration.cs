﻿using Acme.Bidding.Core.Application.Services;
using Acme.Bidding.Core.Domain.Configurations;
using Acme.Bidding.Core.Domain.Mappings;
using Acme.Bidding.Core.Domain.Repositories;
using Acme.Bidding.Core.Domain.Services.Application;
using Acme.Bidding.Core.Domain.Services.Helper;
using Acme.Bidding.Infrastructure.Persistence.Repositories;
using Acme.Bidding.Infrastructure.Utility.Services;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;

namespace Acme.Bidding.WebAPI.Configuration
{
    /// <summary>
    /// IOC contaner start-up configuration
    /// </summary>
    public static class IocContainerConfiguration
    {
        public static void ConfigureService(IServiceCollection services)
        {
            MapperConfiguration mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = new ApiVersion(1, 0);
                config.AssumeDefaultVersionWhenUnspecified = true;
                config.ReportApiVersions = true;
                config.ApiVersionReader = ApiVersionReader.Combine(new HeaderApiVersionReader("api-version"), new QueryStringApiVersionReader("api-version"));
            });
            services.AddVersionedApiExplorer();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<ITokenAttributesService, TokenAttributesService>();
            services.AddSingleton<IBroadcastService, BroadcastService>();
            services.AddScoped<ILoggerService, SerilogLoggerService>();

            services.AddScoped<IAcmeBiddingRepository, AcmeBiddingRepository>();
            services.AddScoped<IAcmeBiddingRepository, AcmeBiddingRepository>();

            services.AddScoped<IItemService, ItemService>();
            services.AddScoped<IItemBidService, ItemBidService>();
            services.AddScoped<IUserService, UserService>();


            services.AddHttpClient("AuthServiceApi", c =>
            {
                c.BaseAddress = new Uri(services.BuildServiceProvider().GetService<IOptions<AppSettings>>().Value.IdentityServerAuthorityUrl);
            });
        }
    }
}