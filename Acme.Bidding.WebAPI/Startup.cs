using Acme.Bidding.Core.Domain.Configurations.Swagger;
using Acme.Bidding.Core.Domain.Hubs;
using Acme.Bidding.Core.Domain.Middleware;
using Acme.Bidding.Infrastructure.Persistence;
using Acme.Bidding.WebAPI.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;

namespace Acme.Bidding.WebAPI
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public string ContentRootPath { get; }

        public Startup(IConfiguration configuration, IHostEnvironment env)
        {
            Configuration = configuration;
            ContentRootPath = env.ContentRootPath;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            ConfigurationOptions.ConfigureService(services, Configuration);
            IocContainerConfiguration.ConfigureService(services);
            SwaggerConfiguration.ConfigureService(services, ContentRootPath);

            services.AddAuthentication(o =>
            {
                o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;

            })
            .AddIdentityServerAuthentication(o =>
            {
                o.Authority = Configuration.GetSection("AppSettings:IdentityServerAuthorityUrl").Value;
                o.RequireHttpsMetadata = Convert.ToBoolean(Configuration.GetSection("AppSettings:IdentityServerRequireHttpsMetadata").Value);
                o.ApiName = Configuration.GetSection("AppSettings:IdentityServerApiResourceName").Value;
                o.SaveToken = true;
            });

            services.AddControllers();
            services.AddDbContext<AcmeBiddingDbContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("AcmeBiddingContext"))
                    .EnableSensitiveDataLogging());

            services.AddSignalR();

            services.AddControllers().AddNewtonsoftJson(x => x.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<GlobalLoggerMiddleware>();

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            SwaggerConfiguration.Configure(app);

            app.UseCors(config =>
            {
                config.AllowCredentials();
                config.AllowAnyHeader();
                config.AllowAnyMethod();
                config.AllowAnyOrigin();
                config.WithOrigins("http://localhost:4200", "https://localhost:4200");
            });

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<AcmeSignalRHub>("/acmeSignalRHub");
            });
        }
    }
}
