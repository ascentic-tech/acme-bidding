﻿using System.Threading.Tasks;
using Twilio.Rest.Verify.V2.Service;

namespace Acme.Bidding.Auth.Core.Domain.Services
{
    public interface IPhoneVerificationSenderService
    {
        Task<VerificationResource> SendVeryficationCode(string phoneNumber);
        Task<VerificationCheckResource> VerifyCode(string phoneNumber, string code);
    }
}
