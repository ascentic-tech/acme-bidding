﻿using Acme.Bidding.Core.Domain.Services.Helper;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.Bidding.Infrastructure.Utility.Services
{
    public class SerilogLoggerService : ILoggerService
    {
        public void LogError(string message)
        {
            Log.Logger.Error(message);
        }

        public void LogError(string message, Exception exception)
        {
            Log.Logger.Error(message, exception);
        }

        public void LogInfo(string message)
        {
            Log.Logger.Information(message);
        }
    }
}
