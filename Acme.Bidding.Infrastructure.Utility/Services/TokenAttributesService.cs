﻿using Acme.Bidding.Core.Domain.Services.Helper;
using Microsoft.AspNetCore.Http;
using System;

namespace Acme.Bidding.Infrastructure.Utility.Services
{
    public class TokenAttributesService : ITokenAttributesService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public TokenAttributesService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public bool IsAuthenticated
        {
            get
            {
              return _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated;
            }
        }

        public Guid IdentityUserId
        {
            get
            {
                return IsAuthenticated ? Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirst(c => c.Type == "sub").Value) : Guid.Empty;
            }
        }

        public string UserEmail
        {
            get
            {
                return IsAuthenticated ? _httpContextAccessor.HttpContext.User.FindFirst(c => c.Type == "email").Value : null;
            }
        }
    }
}
