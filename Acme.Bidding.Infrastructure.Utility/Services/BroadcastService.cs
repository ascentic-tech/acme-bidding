﻿using Acme.Bidding.Core.Domain.Hubs;
using Acme.Bidding.Core.Domain.Services.Helper;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;

namespace Acme.Bidding.Infrastructure.Utility.Services
{
    public class BroadcastService : IBroadcastService
    {
        private readonly IHubContext<AcmeSignalRHub> _hubContext;

        public BroadcastService(IHubContext<AcmeSignalRHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public void BroadcastData(string hubMethod, object data)
        {
            _hubContext.Clients.All.SendAsync(hubMethod, JsonConvert.SerializeObject(data));
        }

    }
}
