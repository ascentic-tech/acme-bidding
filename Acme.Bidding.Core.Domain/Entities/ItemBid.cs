﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Acme.Bidding.Core.Domain.Entities
{
    public class ItemBid : BaseEntity, IAuditable
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int ItemId { get; set; }
        public virtual Item Item { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal BidAmount { get; set; }
        public DateTime BiddedAt { get; set; }      
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
    }
}
