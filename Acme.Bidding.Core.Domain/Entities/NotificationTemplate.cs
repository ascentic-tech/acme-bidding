﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Acme.Bidding.Core.Domain.Entities
{
    public class NotificationTemplate : BaseEntity, IAuditable
    {
        public string Tittle { get; set; }
        public string Body { get; set; }
        public int NotificationTypeId { get; set; }
        public virtual NotificationType NotificationType { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
        public virtual ICollection<Notification> Notifications { get; set; }

        public NotificationTemplate()
        {
            Notifications = new Collection<Notification>();
        }
    }
}
