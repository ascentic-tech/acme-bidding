﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.Bidding.Core.Domain.Entities
{
    public class RolePermission : BaseEntity, IAuditable
    {
        public int PermissionId { get; set; }
        public virtual Permission Permission { get; set; }
        public int RoleId { get; set; }
        public virtual Role Role { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
    }
}
