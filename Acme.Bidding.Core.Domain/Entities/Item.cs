﻿using Acme.Bidding.Core.Domain.Common;
using Acme.Bidding.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Acme.Bidding.Core.Domain.Entities
{
    public class Item : BaseEntity, IAuditable
    {
        public string Title { get; set; }
        public string Description { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal StartingBid { get; set; }
        public DateTime StartingTime { get; set; }
        public DateTime Duration { get; set; }
        public ItemStatus ItemStatus { get; set; }
        public virtual ICollection<ItemBid> ItemBids { get; set; }
        public virtual ICollection<ItemImage> ItemImages { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }

        public Item()
        {
            ItemBids = new Collection<ItemBid>();
            ItemImages = new Collection<ItemImage>();
        }
    }
}
