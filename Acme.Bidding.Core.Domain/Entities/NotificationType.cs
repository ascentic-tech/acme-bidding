﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Acme.Bidding.Core.Domain.Entities
{
    public class NotificationType : BaseEntity, IAuditable
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
        public virtual ICollection<NotificationTemplate> NotificationTemplates { get; set; }

        public NotificationType()
        {
            NotificationTemplates = new Collection<NotificationTemplate>();
        }
    }
}
