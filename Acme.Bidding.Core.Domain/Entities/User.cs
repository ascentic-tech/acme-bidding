﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Acme.Bidding.Core.Domain.Entities
{
    public class User : BaseEntity, IAuditable
    {
        public Guid IdentityUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }

        public virtual ICollection<UserNotification> UserNotifications { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<ItemBid> ItemBids { get; set; }

        public User()
        {
            UserNotifications = new Collection<UserNotification>();
            UserRoles = new Collection<UserRole>();
            ItemBids = new Collection<ItemBid>();
        }
    }
}
