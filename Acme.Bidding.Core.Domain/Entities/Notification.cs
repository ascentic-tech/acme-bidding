﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Acme.Bidding.Core.Domain.Entities
{
    public class Notification : BaseEntity, IAuditable
    {
        public string NotificationContent { get; set; }
        public int NotificationTemplateId { get; set; }
        public virtual NotificationTemplate NotificationTemplate { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
        public virtual ICollection<UserNotification> UserNotifications { get; set; }

        public Notification()
        {
            UserNotifications = new Collection<UserNotification>();
        }
    }
}
