﻿using System.ComponentModel;

namespace Acme.Bidding.Core.Domain.Enums
{
    public enum ItemStatus
    {
        [Description("New")]
        New = 1,
        [Description("Active")]
        Active = 2,
        [Description("Scheduled")]
        Sold = 3,
        [Description("Sold")]
        Scheduled = 4,
        [Description("Unsold")]
        Unsold = 5,
        [Description("Deleted")]
        Deleted = 6
    }
}
