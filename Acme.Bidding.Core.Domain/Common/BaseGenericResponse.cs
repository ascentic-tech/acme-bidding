﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.Bidding.Core.Domain.Common
{
    public class BaseGenericResponse<T> 
    {
        public T Data { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }       
        public Paging Paging { get; set; }
    }
}



