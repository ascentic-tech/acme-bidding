﻿namespace Acme.Bidding.Core.Domain.Common
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }

}
