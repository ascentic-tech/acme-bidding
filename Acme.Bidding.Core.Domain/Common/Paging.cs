﻿namespace Acme.Bidding.Core.Domain.Common
{
    public class Paging
    {
        public int? PageNumber { get; set; }
        public int? PageSize { get; set; }
        public int TotalRecords { get; set; }
    }
}



