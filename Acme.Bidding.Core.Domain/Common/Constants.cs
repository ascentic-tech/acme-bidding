﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.Bidding.Core.Domain.Common
{
    public static class Constants
    {
        public static class ResponseMessages
        {
            public const string AddItemBidFailure_LowerBid = "Please enter higher bid";
            public const string SomethingWentWrong = "Something went wrong. Please try again later.";
            public const string ItemsNotFound = "No items found.";
            public const string ItemsNotSaved = "Item not saved.";
            public const string ItemsNotUpdated = "Item not updated.";
            public const string ItemBidsNotFound = "No item bids found.";
            public const string ItemBidsNotSaved = "Item bids not saved.";
            public const string ItemBidsNotUpdated = "Item bids not updated.";
            public const string AddUserFailure_Identity = "Refused by authentication service";
        }

    }
}
