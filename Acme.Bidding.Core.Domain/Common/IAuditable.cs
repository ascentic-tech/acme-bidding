﻿using System;

namespace Acme.Bidding.Core.Domain.Common
{
    public interface IAuditable
    {
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
    }

}
