﻿using System.ComponentModel.DataAnnotations;

namespace Acme.Bidding.Core.Domain.Configurations
{
    public class AppSettings : IValidatable
    {
        /// <summary>
        /// API name section
        /// </summary>
        public string ApiName { get; set; }

        /// <summary>
        /// API version section
        /// </summary>
        public string ApiVersion { get; set; }

        /// <summary>
        /// API display name section
        /// </summary>
        public string ApiDisplayName { get; set; }

        /// <summary>
        /// API description section
        /// </summary>
        public string ApiDescription { get; set; }

        /// <summary>
        /// Contact name section
        /// </summary>
        public string ContactName { get; set; }

        /// <summary>
        /// Contact name section
        /// </summary>
        public string ContactEmail { get; set; }

        /// <summary>
        /// Contact name section
        /// </summary>
        public string ContactUrl { get; set; }

        public string ActionApiUrl { get; set; }

        public string PushNotificationApiUrl { get; set; }

        public string BackgroundWorkerUrl { get; set; }

        public string CoreApiUrl { get; set; }

        public string ClickAction { get; set; }

        /// <summary>
        /// Identity Server URL
        /// </summary>
        public string IdentityServerAuthorityUrl { get; set; }

        public string AuthApiUrl { get; set; }

        /// <summary>
        /// API Resource Name on Identity Server
        /// </summary>
        public string IdentityServerApiResourceName { get; set; }

        /// <summary>
        /// Make Identity Server HTTPS Meta Data Required
        /// </summary>
        public bool IdentityServerRequireHttpsMetadata { get; set; }

        /// <summary>
        /// API Secret
        /// </summary>
        public string IdentityServerApiSecret { get; set; }

        public string EmailApiUrl { get; set; }

        public string SriLankaTimeZone { get; set; }

        public void Validate()
        {
            Validator.ValidateObject(this, new ValidationContext(this), true);
        }
    }
}
