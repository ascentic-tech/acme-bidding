﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;


namespace Acme.Bidding.Core.Domain.Configurations.Swagger
{
    public static class SwaggerConfiguration
    {
        /// <summary>
        /// Configures the service.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="contentRootPath"></param>
        public static void ConfigureService(IServiceCollection services, string contentRootPath)
        {
            SwaggerSettings swaggerAppSettings = services.BuildServiceProvider().GetService<SwaggerSettings>();

            services.AddSwaggerGen(c =>
            {
                var provider = services.BuildServiceProvider()
                        .GetRequiredService<IApiVersionDescriptionProvider>();

                foreach (var description in provider.ApiVersionDescriptions)
                {
                    c.SwaggerDoc($"v{description.GroupName}", CreateInfoForApiVersion(swaggerAppSettings, description, File.ReadAllText(Path.Combine(contentRootPath, $"Docs/api-{$"v{description.GroupName}"}-description.md"))));
                }
                c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());

                c.DocInclusionPredicate((version, desc) =>
                {
                    var versions = desc.CustomAttributes()
                           .OfType<ApiVersionAttribute>()
                           .SelectMany(attr => attr.Versions);

                    if (versions.Any(v => $"v{v.ToString()}" == version) && version == "v1.0")
                    {
                        return true;
                    }

                    var maps = desc.CustomAttributes()
                        .OfType<MapToApiVersionAttribute>()
                        .SelectMany(attr => attr.Versions)
                        .ToArray();

                    return version != "v1.0" && versions.Any(v => $"v{v}" == version) &&
                    maps.Any(v => $"v{v}" == version);
                });

                string xmlFile = $"{Assembly.GetEntryAssembly()?.GetName().Name}.xml";
                string xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,

                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement { { new OpenApiSecurityScheme { Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" } }, new string[] { } } });
            });
        }

        /// <summary>
        /// Configures the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        public static void Configure(IApplicationBuilder app)
        {
            SwaggerSettings swaggerAppSettings = app.ApplicationServices.GetService<SwaggerSettings>();
            app.UseRewriter(new RewriteOptions().AddRedirect("^$", "docs"));
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                foreach (var version in swaggerAppSettings.ApiVersions)
                {
                    c.SwaggerEndpoint($"/swagger/{version}/swagger.json", $"{swaggerAppSettings.ApiDisplayName} {version}");              
                }
                c.DocumentTitle = $"{swaggerAppSettings.ApiDisplayName} Docs (Swagger UI)";
                c.HeadContent = new StringBuilder(c.HeadContent).AppendLine($"<img src='{swaggerAppSettings.LogoUrl}'/>").ToString();
                c.RoutePrefix = "docs";

            });

            app.UseReDoc(c =>
            {
                foreach (var version in swaggerAppSettings.ApiVersions)
                {
                    c.SpecUrl = $"/swagger/{version}/swagger.json";
                }
                c.DocumentTitle = $"{swaggerAppSettings.ApiDisplayName} Docs (ReDoc)";
                c.HeadContent = new StringBuilder(c.HeadContent).AppendLine($"<img src='{swaggerAppSettings.LogoUrl}'/>").ToString();
            });
        }

        private static OpenApiInfo CreateInfoForApiVersion(SwaggerSettings swaggerAppSettings, ApiVersionDescription description, string descriptionText)
        {
            var info = new OpenApiInfo
            {
                Title = swaggerAppSettings.ApiDisplayName,
                Version = $"v{description.GroupName}",
                Description = descriptionText,
                Contact = new OpenApiContact
                {
                    Name = swaggerAppSettings.ContactName,
                    Email = swaggerAppSettings.ContactEmail,
                    Url = new Uri(swaggerAppSettings.ContactUrl)
                },
            };

            if (description.IsDeprecated)
            {
                info.Description = " This API version has been deprecated.";
            }

            return info;
        }

    }
}