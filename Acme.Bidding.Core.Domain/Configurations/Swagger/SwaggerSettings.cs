﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Acme.Bidding.Core.Domain.Configurations.Swagger
{
    public class SwaggerSettings : IValidatable
    {
        /// <summary>
        /// API name section
        /// </summary>
        public string ApiName { get; set; }

        /// <summary>
        /// API version section
        /// </summary>
        public List<string> ApiVersions { get; set; }

        /// <summary>
        /// API display name section
        /// </summary>
        public string ApiDisplayName { get; set; }

        /// <summary>
        /// API description section
        /// </summary>
        public string ApiDescription { get; set; }

        /// <summary>
        /// Contact name section
        /// </summary>
        public string ContactName { get; set; }

        /// <summary>
        /// Contact name section
        /// </summary>
        public string ContactEmail { get; set; }

        /// <summary>
        /// Contact name section
        /// </summary>
        public string ContactUrl { get; set; }

        public string LogoUrl { get; set; }

        public void Validate()
        {
            Validator.ValidateObject(this, new ValidationContext(this), true);
        }
    }
}
