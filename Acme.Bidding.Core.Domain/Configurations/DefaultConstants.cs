﻿namespace Acme.Bidding.Core.Domain.Configurations
{
    public static class DefaultConstants
    {
        /// <summary>
        /// Name of the connection string
        /// </summary>
        public const string DefaultConnection = nameof(DefaultConstants.DefaultConnection);

        /// <summary>
        /// Connection string section
        /// </summary>
        public const string ConnectionStrings = nameof(DefaultConstants.ConnectionStrings);

        /// <summary>
        /// Kerstel configuration section
        /// </summary>
        public const string Kestrel = nameof(Kestrel);

        /// <summary>
        /// Name of the AppSettings section
        /// </summary>
        public const string AppSettings = nameof(DefaultConstants.AppSettings);

        /// <summary>
        /// Name of the SwaggerSettings section
        /// </summary>
        public const string SwaggerSettings = nameof(DefaultConstants.SwaggerSettings);

        /// <summary>
        /// Name of the Authentication settings section
        /// </summary>
        public const string AuthenticationSettings = nameof(DefaultConstants.AuthenticationSettings);


        /// <summary>
        /// Name of the logging section in the config files
        /// </summary>
        public const string Logging = nameof(DefaultConstants.Logging);
    }
}
