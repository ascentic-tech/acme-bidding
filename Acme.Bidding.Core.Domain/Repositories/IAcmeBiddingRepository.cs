﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Acme.Bidding.Core.Domain.Repositories
{
    public interface IAcmeBiddingRepository
    {

        Task<T> AddAsync<T>(T entity) where T : BaseEntity;
        Task<IEnumerable<T>> AddRangeAsync<T>(IEnumerable<T> entities) where T : BaseEntity;
        Task<T> UpdateAsync<T>(object key, T entity) where T : BaseEntity;
        Task DeleteAsync<T>(T entity) where T : BaseEntity;
        Task DeleteRange<T>(IEnumerable<T> entities) where T : BaseEntity;
        Task<T> GetByIdAsync<T>(int id) where T : BaseEntity;
        Task<IEnumerable<T>> GetAllAsync<T>() where T : BaseEntity;
        Task<IEnumerable<T>> GetAllWithPagingAsync<T>(int page, int pageSize) where T : BaseEntity;
        Task<IEnumerable<T>> GetAllIncludingWithPagingAsync<T>(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity;
        Task<IEnumerable<T>> GetAllIncludingAsync<T>(params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity;
        Task<T> FindAsync<T>(Expression<Func<T, bool>> predicate) where T : BaseEntity;
        Task<T> FindIncludingAsync<T>(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity;
        Task<IEnumerable<T>> FindAllAsync<T>(Expression<Func<T, bool>> predicate) where T : BaseEntity;
        Task<IEnumerable<T>> FindAllIncludingAsync<T>(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity;
        Task<IEnumerable<T>> FindAllWithPagingAsync<T>(Expression<Func<T, bool>> predicate, Expression<Func<T, object>> orderBy, bool orderByAscending, int page, int pageSize) where T : BaseEntity;
        Task<IEnumerable<T>> FindAllIncludingWithPagingAsync<T>(Expression<Func<T, bool>> predicate, Expression<Func<T, object>> orderBy, bool orderByAscending, int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity;
        IEnumerable<T> FindAllIncludingWithPagingGroupBy<T>(Expression<Func<T, bool>> predicate, Expression<Func<T, object>> orderBy, Expression<Func<T, object>> groupBy, bool orderByAscending, int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity;
        IEnumerable<IGrouping<object, T>> FindAllIncludingWithGroupBy<T>(Expression<Func<T, bool>> predicate, Expression<Func<T, object>> groupBy, params Expression<Func<T, object>>[] includeProperties) where T : BaseEntity;
        Task<int> FindAllCountAsync<T>(Expression<Func<T, bool>> predicate) where T : BaseEntity;
        Task<int> FindAllCountAsync<T>() where T : BaseEntity;
    }
}