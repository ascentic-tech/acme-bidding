﻿using Acme.Bidding.Core.Domain.Common;
using Acme.Bidding.Core.Domain.Services.Helper;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static Acme.Bidding.Core.Domain.Common.Constants;

namespace Acme.Bidding.Core.Domain.Middleware
{
    public class GlobalLoggerMiddleware
    {
        private readonly RequestDelegate _next;
        public GlobalLoggerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            ILoggerService _loggerService = httpContext.RequestServices.GetService(typeof(ILoggerService)) as ILoggerService;

            try
            {
                var request = await FormatRequestAsync(httpContext.Request);
                var originalBodyStream = httpContext.Response.Body;

                using (var responseBody = new MemoryStream())
                {
                    httpContext.Response.Body = responseBody;
                    await _next(httpContext);
                    var response = await FormatResponseAsync(httpContext.Response);

                    _loggerService.LogInfo(request);
                    _loggerService.LogInfo(response);

                    await responseBody.CopyToAsync(originalBodyStream);
                }
            }
            catch (Exception ex)
            {
                _loggerService.LogError(ex.Message, ex);
                await HandleGlobalExceptionAsync(httpContext);
            }
        }

        private async Task<string> FormatRequestAsync(HttpRequest request)
        {
            var body = request.Body;
            request.EnableBuffering();
            var buffer = new byte[Convert.ToInt32(request.ContentLength)];
            await request.Body.ReadAsync(buffer, 0, buffer.Length);
            var bodyAsText = Encoding.UTF8.GetString(buffer);
            request.Body = body;
            return $"{request.Scheme} {request.Host}{request.Path} {request.QueryString} {bodyAsText}";
        }

        private async Task<string> FormatResponseAsync(HttpResponse response)
        {
            response.Body.Seek(0, SeekOrigin.Begin);
            string text = await new StreamReader(response.Body).ReadToEndAsync();
            response.Body.Seek(0, SeekOrigin.Begin);

            return $"{response.StatusCode}: {text}";
        }


        private static Task HandleGlobalExceptionAsync(HttpContext context)
        {
            var result = JsonConvert.SerializeObject(new BaseGenericResponse<object>()
            {
                IsSuccess = false,
                Message = ResponseMessages.SomethingWentWrong
            }, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            return context.Response.WriteAsync(result);
        }
    }
}
