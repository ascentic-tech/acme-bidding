﻿using Acme.Bidding.Core.Domain.Entities;
using Acme.Bidding.Core.Domain.Repositories;
using Acme.Bidding.Core.Domain.Services;
using Acme.Bidding.Core.Domain.Services.Helper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Acme.Bidding.Core.Domain.Attributes
{
    [AttributeUsage(AttributeTargets.All)]
    public class AcmeAuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        private readonly string _permisionCode;
        public AcmeAuthorizeAttribute(string permisionCode)
        {
            _permisionCode = permisionCode;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            ITokenAttributesService _tokenAttributesService = context.HttpContext.RequestServices.GetService(typeof(ITokenAttributesService)) as ITokenAttributesService;
            IAcmeBiddingRepository _acmeBiddingRepository = context.HttpContext.RequestServices.GetService(typeof(IAcmeBiddingRepository)) as IAcmeBiddingRepository;

            List<RolePermission> rolePermission = _acmeBiddingRepository
            .FindIncludingAsync<Permission>(p1 => p1.Code == _permisionCode,
                                                   a => a.RolePermissions)?.Result?.RolePermissions?.ToList();

            List<UserRole> userRoles = _acmeBiddingRepository
                .FindAllAsync<UserRole>(ur => ur.User.IdentityUserId == _tokenAttributesService.IdentityUserId)?.Result?.ToList();

            if (userRoles != null && rolePermission != null)
            {
                var hasPermission = userRoles.Select(ur => ur.RoleId)
                    .Intersect(rolePermission.Select(rp => rp.RoleId)).Any();

                if (!hasPermission)
                {
                    context.Result = new StatusCodeResult(403);
                }
            }
            else
            {
                context.Result = new StatusCodeResult(403);
            }
        }
    }
}