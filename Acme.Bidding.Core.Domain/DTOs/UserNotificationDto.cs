﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.Bidding.Core.Domain.DTOs
{
    public class UserNotificationDto : BaseDto
    {
        public int UserId { get; set; }
        public UserDto User { get; set; }
        public int NotificationId { get; set; }
        public NotificationDto Notification { get; set; }
        public DateTime ViewedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
    }
}
