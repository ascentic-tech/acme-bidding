﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Acme.Bidding.Core.Domain.DTOs
{
    public class NotificationTypeDto : BaseDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
        public ICollection<NotificationTemplateDto> NotificationTemplates { get; set; }

        public NotificationTypeDto()
        {
            NotificationTemplates = new Collection<NotificationTemplateDto>();
        }
    }
}
