﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Acme.Bidding.Core.Domain.DTOs
{
    public class UserDto : BaseDto
    {
        public Guid IdentityUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }

        public ICollection<UserNotificationDto> UserNotifications { get; set; }
        public ICollection<UserRoleDto> UserRoles { get; set; }
        public ICollection<ItemBidDto> ItemBids { get; set; }

        public UserDto()
        {
            UserNotifications = new Collection<UserNotificationDto>();
            UserRoles = new Collection<UserRoleDto>();
            ItemBids = new Collection<ItemBidDto>();
        }
    }
}

