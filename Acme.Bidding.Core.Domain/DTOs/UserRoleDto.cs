﻿using Acme.Bidding.Core.Domain.Common;
using System;

namespace Acme.Bidding.Core.Domain.DTOs
{
    public class UserRoleDto : BaseDto
    {
        public int UserId { get; set; }
        public UserDto User { get; set; }
        public int RoleId { get; set; }
        public RoleDto Role { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
    }
}
