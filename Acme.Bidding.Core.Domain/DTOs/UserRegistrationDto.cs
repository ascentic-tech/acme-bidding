﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Acme.Bidding.Core.Domain.DTOs
{
    public class UserRegistrationDto
    {
        public Guid IdentityUserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string RedirectUrl { get; set; }
    }
}