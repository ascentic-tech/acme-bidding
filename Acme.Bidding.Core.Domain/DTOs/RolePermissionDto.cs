﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.Bidding.Core.Domain.DTOs
{
    public class RolePermissionDto : BaseDto
    {
        public int PermissionId { get; set; }
        public PermissionDto Permission { get; set; }
        public int RoleId { get; set; }
        public RoleDto Role { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
    }
}
