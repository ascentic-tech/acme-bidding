﻿using Acme.Bidding.Core.Domain.Common;
using Acme.Bidding.Core.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Acme.Bidding.Core.Domain.DTOs
{
    public class ItemDto : BaseDto
    {  
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal StartingBid { get; set; }
        public decimal HighestBid { get; set; }
        public DateTime StartingTime { get; set; }
        public DateTime Duration { get; set; }
        public ItemStatus ItemStatus { get; set; }
        public ICollection<ItemBidDto> ItemBids { get; set; }
        public ICollection<ItemImageDto> ItemImages { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }

        public ItemDto()
        {
            ItemBids = new Collection<ItemBidDto>();
            ItemImages = new Collection<ItemImageDto>();
        }
    }
}
