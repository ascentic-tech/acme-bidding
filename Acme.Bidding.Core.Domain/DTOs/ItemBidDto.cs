﻿using Acme.Bidding.Core.Domain.Common;
using System;

namespace Acme.Bidding.Core.Domain.DTOs
{
    public class ItemBidDto : BaseDto
    {
        public int UserId { get; set; }
        public UserDto User { get; set; }
        public int ItemId { get; set; }
        public ItemDto Item { get; set; }
        public decimal BidAmount { get; set; }
        public DateTime BiddedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
    }
}
