﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Acme.Bidding.Core.Domain.DTOs
{
    public class NotificationTemplateDto : BaseDto
    {
        public string Tittle { get; set; }
        public string Body { get; set; }
        public int NotificationTypeId { get; set; }
        public  NotificationTypeDto NotificationType { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
        public  ICollection<NotificationDto> Notifications { get; set; }

        public NotificationTemplateDto()
        {
            Notifications = new Collection<NotificationDto>();
        }
    }
}
