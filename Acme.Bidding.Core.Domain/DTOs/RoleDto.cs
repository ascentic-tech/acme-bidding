﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Acme.Bidding.Core.Domain.DTOs
{
    public class RoleDto : BaseDto
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
        public ICollection<UserRoleDto> UserRoles { get; set; }
        public ICollection<RolePermissionDto> RolePermissions { get; set; }

        public RoleDto()
        {
            UserRoles = new Collection<UserRoleDto>();
            RolePermissions = new Collection<RolePermissionDto>();
        }
    }
}
