﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Acme.Bidding.Core.Domain.DTOs
{
    public class NotificationDto : BaseDto
    {
        public string NotificationContent { get; set; }
        public int NotificationTemplateId { get; set; }
        public  NotificationTemplateDto NotificationTemplate { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
        public  ICollection<UserNotificationDto> UserNotifications { get; set; }

        public NotificationDto()
        {
            UserNotifications = new Collection<UserNotificationDto>();
        }
    }
}
