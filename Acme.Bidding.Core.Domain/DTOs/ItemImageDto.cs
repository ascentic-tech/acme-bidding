﻿using Acme.Bidding.Core.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Acme.Bidding.Core.Domain.DTOs
{
    public class ItemImageDto : BaseDto
    {
        public int ItemId { get; set; }
        public ItemDto Item { get; set; }
        public string Url { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
        public int? LastModifiedBy { get; set; }
    }
}
