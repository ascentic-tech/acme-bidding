﻿using Acme.Bidding.Core.Domain.Common;
using Acme.Bidding.Core.Domain.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Acme.Bidding.Core.Domain.Services.Application
{
    public interface IUserService
    {
        Task<BaseGenericResponse<UserDto>> AddUserAsync(UserRegistrationDto userRegistrationDto);
        Task<UserDto> UpdateUserAsync(int id, UserDto user);
    }
}