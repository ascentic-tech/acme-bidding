﻿using Acme.Bidding.Core.Domain.Common;
using Acme.Bidding.Core.Domain.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Acme.Bidding.Core.Domain.Services.Application
{
    public interface IItemService
    {
        Task<BaseGenericResponse<ItemDto>> GetItemByIdAsync(int id);
        Task<BaseGenericResponse<IEnumerable<ItemDto>>> GetItemListByStatusAsync(int itemStatus, int page, int? pageSize);
        Task<BaseGenericResponse<IEnumerable<ItemDto>>> GetAllItemListAsync(int page, int? pageSize);
        Task<ItemDto> AddItemAsync(ItemDto item);
        Task<ItemDto> UpdateItemAsync(int id, ItemDto item);
    }
}