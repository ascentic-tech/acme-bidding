﻿using Acme.Bidding.Core.Domain.Common;
using Acme.Bidding.Core.Domain.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Acme.Bidding.Core.Domain.Services.Application
{
    public interface IItemBidService
    {
        Task<BaseGenericResponse<IEnumerable<ItemBidDto>>> GetBidListByItemIdAsync(int itemId, int? page, int? pageSize);
        Task<BaseGenericResponse<IEnumerable<ItemBidDto>>> GetBidListByUserIdAsync(int userId, int? page, int? pageSize);
        Task<BaseGenericResponse<IEnumerable<ItemBidDto>>> GetBidListByUserAndItemAsync(int userId, int itemId, int page, int? pageSize);
        Task<BaseGenericResponse<decimal>> AddItemBidAsync(ItemBidDto itemBid);
    }
}