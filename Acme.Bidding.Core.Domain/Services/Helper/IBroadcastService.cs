﻿using Acme.Bidding.Core.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Acme.Bidding.Core.Domain.Services.Helper
{
    public interface IBroadcastService
    {
        void BroadcastData(string hubMethod, object data);
    }
}
