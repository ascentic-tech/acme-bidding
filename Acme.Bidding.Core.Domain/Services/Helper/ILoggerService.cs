﻿using System;

namespace Acme.Bidding.Core.Domain.Services.Helper
{
    public interface ILoggerService
    {
         void LogInfo(string message);
         void LogError(string message);
         void LogError(string message,Exception exception);
    }
}