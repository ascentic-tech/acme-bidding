﻿using System;

namespace Acme.Bidding.Core.Domain.Services.Helper
{
    public interface ITokenAttributesService
    {
        bool IsAuthenticated { get; }
        Guid IdentityUserId { get; }
        string UserEmail { get; }
    }
}
