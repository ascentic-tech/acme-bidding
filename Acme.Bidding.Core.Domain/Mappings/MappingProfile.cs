using Acme.Bidding.Core.Domain.DTOs;
using Acme.Bidding.Core.Domain.Entities;
using AutoMapper;


namespace Acme.Bidding.Core.Domain.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ItemBid, ItemBidDto>().MaxDepth(3);
            CreateMap<ItemBidDto, ItemBid>().MaxDepth(3);

            CreateMap<Item, ItemDto>().PreserveReferences();
            CreateMap<ItemDto, Item>().PreserveReferences();

            CreateMap<ItemImage, ItemImageDto>().PreserveReferences();
            CreateMap<ItemImageDto, ItemImage>().PreserveReferences();

            CreateMap<Notification, NotificationDto>().PreserveReferences();
            CreateMap<NotificationDto, Notification>().PreserveReferences();

            CreateMap<NotificationTemplate, NotificationTemplateDto>().PreserveReferences();
            CreateMap<NotificationTemplateDto, NotificationTemplate>().PreserveReferences();

            CreateMap<NotificationType, NotificationTypeDto>().PreserveReferences();
            CreateMap<NotificationTypeDto, NotificationType>().PreserveReferences();

            CreateMap<Permission, PermissionDto>().PreserveReferences();
            CreateMap<PermissionDto, Permission>().PreserveReferences();

            CreateMap<Role, RoleDto>().PreserveReferences();
            CreateMap<RoleDto, Role>().PreserveReferences();

            CreateMap<RolePermission, RolePermissionDto>().PreserveReferences();
            CreateMap<RolePermissionDto, RolePermission>().PreserveReferences();

            CreateMap<User, UserDto>().PreserveReferences();
            CreateMap<UserDto, User>().PreserveReferences();

            CreateMap<User, UserRegistrationDto>().PreserveReferences();
            CreateMap<UserRegistrationDto, User>().PreserveReferences();

            CreateMap<UserNotification, UserNotificationDto>().PreserveReferences();
            CreateMap<UserNotificationDto, UserNotification>().PreserveReferences();

            CreateMap<UserRole, UserRoleDto>().PreserveReferences();
            CreateMap<UserRoleDto, UserRole>().PreserveReferences();
        }
    }
}