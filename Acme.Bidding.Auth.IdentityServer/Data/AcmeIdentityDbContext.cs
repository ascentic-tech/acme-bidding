﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Acme.Bidding.Auth.IdentityServer.Data
{
    public class AcmeIdentityDbContext : IdentityDbContext
    {
        public AcmeIdentityDbContext(DbContextOptions<AcmeIdentityDbContext> options) : base(options) { }
    }
}
