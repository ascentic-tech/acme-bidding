using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace Acme.Bidding.Auth.IdentityServer.Data
{
    public static class Config
    {
        public static List<Client> Clients { get; set; } = new List<Client>
        {
            new Client
            {
                ClientId = "acme-bidding-angular",
                ClientName = "Acme Bidding Angular Client Application",
                RequireClientSecret = false,
                AllowedGrantTypes = GrantTypes.Code,
                RedirectUris = new List<string> {"http://localhost:4200","http://localhost:4200/silent-renew.html"},
                AllowedCorsOrigins= new List<string> {"http://localhost:4200"},
                PostLogoutRedirectUris= new List<string> {"http://localhost:4200"},
                AllowedScopes = new List<string>
                {
                   IdentityServerConstants.StandardScopes.OpenId,
                   IdentityServerConstants.StandardScopes.Profile,
                   IdentityServerConstants.StandardScopes.Email,
                   IdentityServerConstants.StandardScopes.OfflineAccess,
                    "acme-bidding-api"
                },
                AllowOfflineAccess=true,
                RequirePkce = true,
                AllowPlainTextPkce = false,
                AllowAccessTokensViaBrowser = true,
                RequireConsent = false,
                AccessTokenLifetime = 600
             }
        };
        public static List<IdentityResource> IdentityResources { get; set; } = new List<IdentityResource>
        {
            new IdentityResources.OpenId(),
            new IdentityResources.Profile(),
            new IdentityResources.Email(),
        };
        public static List<ApiResource> ApiResources { get; set; } = new List<ApiResource>
        {      
            new ApiResource
            {
                Name="acme-bidding-api",
                DisplayName="Acme Bidding Api",
                Scopes = new List<string>{"acme-bidding-api"}
            }
        };

        public static IEnumerable<ApiScope> ApiScopes { get; set; } = new List<ApiScope>
        {
            new ApiScope("acme-bidding-api", "Access to Acme Bidding Api"),
        };

        public static List<IdentityRole> IdentityRoles { get; set; } = new List<IdentityRole>
        {
            new IdentityRole{Name="Admin",NormalizedName="ADMIN" },
            new IdentityRole{Name="Bidder",NormalizedName="BIDDER" }
        };


        public static List<IdentityUser> IdentityUsers { get; set; } = new List<IdentityUser>
        {
            new IdentityUser
            {
                Email = "bob@b.com",
                PasswordHash="",
                NormalizedEmail = "BOB@B.COM",
                UserName = "bob@b.com",
                NormalizedUserName = "BOB@B.COM",
                PhoneNumber = "+111111111111",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D"),
                LockoutEnabled = false             
            },
            new IdentityUser
            {
                Email = "alice@a.com",
                NormalizedEmail = "ALICE@A.COM",
                UserName = "alice@a.com",
                NormalizedUserName = "ALICE@A.COM",
                PhoneNumber = "+111111111111",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D"),
                LockoutEnabled = false
            },
                new IdentityUser
            {
                Email = "jhon@j.com",
                NormalizedEmail = "JHON@J.COM",
                UserName = "jhon@j.com",
                NormalizedUserName = "JHON@J.COM",
                PhoneNumber = "+111111111111",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D"),
                LockoutEnabled = false
            }, new IdentityUser
            {
                Email = "ben@b.com",
                NormalizedEmail = "BEN@B.COM",
                UserName = "ben@b.com",
                NormalizedUserName = "BEN@B.COM",
                PhoneNumber = "+111111111111",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D"),
                LockoutEnabled = false
            }
        };
    }
}
