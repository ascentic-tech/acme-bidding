﻿using Microsoft.AspNetCore.Identity;

namespace Acme.Bidding.Auth.IdentityServer.Models.Account
{
    public class RegisterResponseViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }

        public RegisterResponseViewModel(IdentityUser user)
        {
            Id = user.Id;
            Email = user.Email;
        }
    }
}
