﻿namespace Acme.Bidding.Auth.IdentityServer.Models.Account
{
    public class ResultCode
    {
        public static short PasswordChanged = 1;
        public static short PasswordCanceled = 2;
    }
}
