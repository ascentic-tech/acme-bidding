﻿namespace Acme.Bidding.Auth.IdentityServer.Models.Account
{
    public class ConfirmPhoneNumberViewModel
    {
        public string PhoneNumber { get; set; }
        public string Code { get; set; }
    }
}
