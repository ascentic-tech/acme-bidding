﻿namespace Acme.Bidding.Auth.IdentityServer.Models.Account
{
    public class LogoutInputModel
    {
        public string LogoutId { get; set; }
    }
}
