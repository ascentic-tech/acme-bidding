using Acme.Bidding.Auth.IdentityServer.Models.Consent;

namespace Acme.Bidding.Auth.IdentityServer.Models.Device
{
    public class DeviceAuthorizationViewModel : ConsentViewModel
    {
        public string UserCode { get; set; }
        public bool ConfirmUserCode { get; set; }
    }
}